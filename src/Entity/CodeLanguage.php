<?php
/**
 * @file
 * ${FILE_DESCRIPTION}
 */

namespace Drupal\codelanguage\Entity;

use Drupal\codelanguage\ConfigEntity\CodeLanguageInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/* @codingStandardsIgnoreStart */
/**
 * Class CodeLanguage.
 *
 * @codingStandardsIgnoreStart
 * @ConfigEntityType(
 *   id = "codelanguage",
 *   label = @Translation("Code Language"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\codelanguage\ConfigEntity\CodeLanguageAddForm",
 *       "edit" = "Drupal\codelanguage\ConfigEntity\CodeLanguageEditForm",
 *       "disable" = "Drupal\codelanguage\ConfigEntity\CodeLanguageDisableForm",
 *       "enable" = "Drupal\codelanguage\ConfigEntity\CodeLanguageEnableForm",
 *       "delete" = "Drupal\codelanguage\ConfigEntity\CodeLanguageDeleteForm"
 *     },
 *     "list_builder" = "Drupal\codelanguage\ConfigEntity\CodeLanguageListBuilder",
 *     "access" = "Drupal\codelanguage\ConfigEntity\CodeLanguageAccessControlHandler"
 *   },
 *   config_prefix = "codelanguage",
 *   admin_permission = "administer codelanguage",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *     "status" = "status"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/codelanguage/add",
 *     "edit-form" = "/admin/structure/codelanguage/manage/{codelanguage}",
 *     "disable" = "/admin/structure/codelanguage/manage/{codelanguage}/disable",
 *     "enable" = "/admin/structure/codelanguage/manage/{codelanguage}/enable",
 *     "delete-form" = "/admin/structure/codelanguage/manage/{codelanguage}/delete"
 *   }
 * )
 * @codingStandardsIgnoreEnd
 *
 * @package Drupal\codelanguage\Entity
 */
class CodeLanguage extends ConfigEntityBase implements CodeLanguageInterface {
  /* @codingStandardsIgnoreEnd */

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = parent::label();

    return $label ?: $this->id();
  }

}
