<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageInterface.
 */

namespace Drupal\codelanguage\ConfigEntity;

use \Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface CodeLanguageInterface.
 */
interface CodeLanguageInterface extends ConfigEntityInterface {

}
