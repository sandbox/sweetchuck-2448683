<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageDeleteForm.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class CodeLanguageEditForm.
 *
 * @package Drupal\codelanguage\ConfigEntity
 */
class CodeLanguageDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $entity_type = $this->entityManager->getDefinition('codelanguage');

    return t(
      'Are you sure you want to delete the @type %label?',
      [
        '@type' => $entity_type->getLabel(),
        '%label' => $this->entity->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url($this->entity->getEntityTypeId() . '.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $form_state->setRedirectUrl($this->getCancelUrl());

    $entity_type = $this->entityManager->getDefinition('codelanguage');
    drupal_set_message($this->t('%label @type has been deleted.', array(
      '%label' => $this->entity->label(),
      '@type' => $entity_type->getLabel(),
    )));
  }

}
