<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageEditForm.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class CodeLanguageEditForm.
 */
class CodeLanguageEditForm extends CodeLanguageAddForm {

  /**
   * Page title.
   *
   * @param CodeLanguageInterface $codelanguage
   *   CodeLanguage config entity being edited.
   *
   * @return string
   *   Page title.
   */
  public function pageTitle(CodeLanguageInterface $codelanguage) {
    return $this->t(
      'Edit %label code language',
      [
        '%label' => $codelanguage->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function submitFormMessage(array &$form, FormStateInterface $form_state) {
    $entity_type = $this->entityManager->getDefinition('codelanguage');
    drupal_set_message($this->t('%label @type has been updated.', array(
      '%label' => $this->entity->label(),
      '@type' => $entity_type->getLabel(),
    )));

    return $this;
  }

}
