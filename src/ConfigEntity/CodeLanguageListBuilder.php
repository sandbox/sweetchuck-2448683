<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageListBuilder.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\String;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Drupal\Core\Config\Entity\DraggableListBuilder;

/**
 * Class CodeLanguageListBuilder.
 */
class CodeLanguageListBuilder extends DraggableListBuilder {

  /**
   * @var string
   */
  protected $weightClass = '';

  /**
   * {@inheritdoc}
   */
  protected $limit = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    parent::__construct($entity_type, $storage);

    // Check if the entity type supports weighting.
    if ($this->entityType->hasKey('weight')) {
      $this->weightClass = Html::getClass($this->weightKey);
    }

    $this->entitiesKey = $entity_type->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codelanguage_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];

    $keys = $this->entityType->getKeys();

    if (!empty($keys['status'])) {
      $header[$keys['status']] = [
        'data' => $this->t('Status'),
      ];
    }

    if (!empty($keys['label'])) {
      $header[$keys['label']] = [
        'data' => $this->t('Name'),
      ];
    }

    $header['description'] = [
      'data' => $this->t('Description'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var CodeLanguageInterface $entity */
    $row = [];

    $keys = $this->entityType->getKeys();

    if (!empty($keys['status'])) {
      $row[$keys['status']] = [
        '#markup' => $entity->status() ? $this->t('Enabled') : $this->t('Disabled'),
      ];
    }

    if (!empty($keys['label'])) {
      $row[$keys['label']] = [
        '#markup' => String::checkPlain($entity->label()),
      ];
    }

    $link = $description = '';

    $url = $entity->get('url');
    if ($url) {
      $link = '<div>' . \Drupal::linkGenerator()->generate($url, Url::fromUri(check_url($url), ['external' => TRUE])) . '</div>';
    }

    $description = $entity->get('description');
    if ($description) {
      $description = '<div>' . String::checkPlain($description) . '</div>';
    }

    $row['description'] = [
      '#markup' => $link . $description,
    ];

    $row += parent::buildRow($entity);

    $row['#attributes']['class'][] = $entity->get('status') ? 'enabled' : 'disabled';
    $row['#attributes']['class'][] = 'codelanguage-id-' . ($entity->id());

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('There is no @label yet.', ['@label' => $this->entityType->getLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $this->weightClass,
        ],
      ],
      '#attributes' => [
        'class' => ['codelanguage-table'],
      ],
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }

    /** @var CodeLanguageInterface $entity */
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);

      if (isset($row[$this->weightKey])) {
        $row[$this->weightKey]['#delta'] = $delta;
      }

      $form[$this->entitiesKey][$entity->id()] = $row;
    }

    if ($this->limit) {
      $form['pager'] = [
        '#theme' => 'pager',
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save order'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && $this->entities[$id]->get($this->weightKey) != $value[$this->weightKey]) {
        $this->entities[$id]->set($this->weightKey, $value[$this->weightKey]);
        $this->entities[$id]->save();
      }
    }
  }

  /**
   * Loads entity IDs without pager.
   *
   * @see https://www.drupal.org/node/2429813
   * @see https://www.drupal.org/node/2425535
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    if ($this->limit) {
      return parent::getEntityIds();
    }

    return $this
      ->getStorage()
      ->getQuery()
      ->execute();
  }

}
