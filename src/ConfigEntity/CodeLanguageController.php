<?php

/**
 * @file
 * Contains \Drupal\codelanguage\Controller\FilterController.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\codelanguage\ConfigEntity\CodeLanguageInterface;

/**
 * Controller routines for filter routes.
 */
class CodeLanguageController {

  /**
   * Gets the label of a Code Language.
   *
   * @param CodeLanguageInterface $code_language
   *   The Code Language object.
   *
   * @return string
   *   The label of the Code Language.
   */
  public function getLabel(CodeLanguageInterface $code_language) {
    return $code_language->label();
  }

}
