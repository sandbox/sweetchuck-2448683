<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageDisableForm.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class CodeLanguageEditForm.
 */
class CodeLanguageDisableForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * @var CodeLanguageInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Disable');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('@todo Code Language disable description.') . "<br />" . __METHOD__;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to disable the Code Language %label?',
      [
        '%label' => $this->entity->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('codelanguage.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->disable()->save();
    drupal_set_message($this->t(
      'Code Language %label is disabled.',
      [
        '%label' => $this->entity->label(),
      ]
    ));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
