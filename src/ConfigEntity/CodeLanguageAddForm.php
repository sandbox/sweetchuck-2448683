<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageAddForm.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CodeLanguageAddForm.
 */
class CodeLanguageAddForm extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var CodeLanguageInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $keys = $this->entity->getEntityType()->getKeys();

    $form[$keys['label']] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#weight' => -30,
    ];

    $form[$keys['id']] = [
      '#type' => 'machine_name',
      '#required' => TRUE,
      '#default_value' => $this->entity->id(),
      '#maxlength' => 255,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => [$keys['label']],
      ],
      '#disabled' => !$this->entity->isNew(),
      '#weight' => -20,
    ];

    $form['url'] = [
      '#type' => 'url',
      '#title' => t('URL'),
      '#default_value' => $this->entity->get('url'),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['extensions'] = [
      '#type' => 'value',
      '#value' => [],
    ];

    $samples = ((array) $this->entity->get('samples')) + [
      'syntax_highlight' => [
        'label' => $this->t('Syntax highlight')
      ],
    ];

    $form['samples'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Samples'),
    ];

    foreach ($samples as $sample_id => $sample) {
      $sample += [
        'id' => $sample_id,
        'label' => $sample_id,
        'code' => '',
      ];

      $form['samples'][$sample_id] = [
        '#tree' => TRUE,
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $sample['label'],
        'id' => [
          '#type' => 'value',
          '#value' => $sample_id,
        ],
        'label' => [
          '#type' => 'textfield',
          '#title' => $this->t('Label'),
          '#required' => TRUE,
          '#default_value' => $sample['label'],
        ],
        'code' => [
          '#type' => 'textarea',
          '#title' => $this->t('Code'),
          '#required' => FALSE,
          '#default_value' => $sample['code'],
          '#rows' => 20,
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->submitFormMessage($form, $form_state);
  }

  /**
   * Determines if the Code Language already exists.
   *
   * @param string $id
   *   The Code Language ID
   *
   * @return bool
   *   TRUE if the Code Language exists, FALSE otherwise.
   */
  public function exists($id) {
    return (bool) $this
      ->entityManager
      ->getStorage($this->entity->getEntityTypeId())
      ->load($id);
  }

  /**
   * Show a status message after a successful submission.
   *
   * @param array $form
   *   Form API render array.
   * @param FormStateInterface $form_state
   *   Form API form state object.
   *
   * @return $this
   *   The called object.
   */
  protected function submitFormMessage(array &$form, FormStateInterface $form_state) {
    $entity_type = $this->entityManager->getDefinition('codelanguage');
    drupal_set_message($this->t('%label @type has been created.', array(
      '%label' => $this->entity->label(),
      '@type' => $entity_type->getLabel(),
    )));

    return $this;
  }

}
