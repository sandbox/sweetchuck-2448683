<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageEnableForm.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class CodeLanguageEditForm.
 */
class CodeLanguageEnableForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * @var CodeLanguageInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Enable');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('@todo Code Language enable description.') . "<br />" . __METHOD__;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to enable the Code Language %label?',
      [
        '%label' => $this->entity->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('codelanguage.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->enable()->save();
    drupal_set_message($this->t(
      'Code Language %label is enabled.',
      [
        '%label' => $this->entity->label(),
      ]
    ));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
