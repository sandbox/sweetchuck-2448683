<?php
/**
 * @file
 * Home of \Drupal\codelanguage\ConfigEntity\CodeLanguageAccessControlHandler.
 */

namespace Drupal\codelanguage\ConfigEntity;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Class CodeLanguageAccessControlHandler.
 */
class CodeLanguageAccessControlHandler extends EntityAccessControlHandler {

}
